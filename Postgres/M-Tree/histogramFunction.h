#include <stdio.h> 
#include <string.h>
#include <stdlib.h>
#include "postgres.h"

#include "access/gist.h"
#include "access/itup.h"
#include "../hystogramTest/histogram.h"
#include "../Utils/definition.h"


Histogram * htg_union(Histogram ** ent, Counter numranges){
	Histogram * uni = (Histogram*) palloc(sizeof(Histogram));
	Dis dMin = PENALTY_LIMIT;
    Dis dMax = 0;
    Dis temp = 0;
    Dis rMax = 0;

    Histogram * key;

    for(Iterator d = 0; d < HTG_SIZE; d++){        
        dMin = PENALTY_LIMIT;
        dMax = 0;
        for(Iterator i = 0; i < numranges; i++){
            key = ent[i];
            temp = key->htg[d] - key->radius;
            if(temp < dMin) dMin = temp;
            temp = key->htg[d] + key->radius;
            if(temp > dMax) dMax = temp;
        }
        uni->htg[d] = (VALH) (dMin + dMax) / 2;
    }

    for(Iterator i = 0; i < numranges; i++){
        key = ent[i];
        Dis dis = histogramDistance(uni,key) + key->radius;
        if(dis > rMax) rMax = dis;
    }

    uni->radius = rMax;

    return uni;
}

