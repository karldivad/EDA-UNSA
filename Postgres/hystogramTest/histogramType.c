#include <stdio.h> 
#include <string.h>	
#include "postgres.h"
#include "fmgr.h"
#include "histogram.h"
#include "../Utils/definition.h"


PG_MODULE_MAGIC;

PG_FUNCTION_INFO_V1(htg_in);

Datum htg_in(PG_FUNCTION_ARGS){
	Path str = PG_GETARG_CSTRING(0);
	BufferLine buffer[BUFFER_SIZE];
	VALH temp;
	Histogram *result;
	
	generateHistogram(str);
	//memcpy(result->path,str,256);

	str = strcat(str,TYPE);
	FILE * hgrFile = fopen(str,"r");
	
	if(hgrFile == NULL){
		ereport(ERROR,
				(errcode(ERRCODE_INVALID_TEXT_REPRESENTATION),
				 errmsg(ERROR_MSG2, 
						str)));
	}
	
	result = (Histogram *) palloc(sizeof(Histogram));

	for(Iterator i = 0; i < HTG_SIZE; i++){
		fgets(buffer,BUFFER_SIZE,hgrFile);	
		sscanf(buffer, "%d", &temp);
		result->htg[i] = temp;
	}

	fclose(hgrFile);

	Command comp2 = (Command) palloc(strlen(str) + strlen(REMOVE_COMMAND) + EXTRA_SPACE);
	comp2 = strcpy(comp2,REMOVE_COMMAND);
	comp2 = strcat(comp2,str);
	SystemError e = system(comp2);
	if(e == SYSTEM_ERROR){
		ereport(ERROR,
				(errcode(ERRCODE_INVALID_TEXT_REPRESENTATION),
				 errmsg(ERROR_MSG3)));	
	}

	result->radius = INITIAL_RADIUS;

	PG_RETURN_POINTER(result);
}

PG_FUNCTION_INFO_V1(htg_out);

Datum htg_out(PG_FUNCTION_ARGS){
	Histogram * htg = (Histogram*) PG_GETARG_POINTER(0);
	
	Message result;
	result = psprintf(HTG_MSG_OUT, htg->htg[0], htg->htg[HTG_SIZE-1], htg->radius);
	
	PG_RETURN_CSTRING(result);
}

PG_FUNCTION_INFO_V1(htg_same);

Datum htg_same(PG_FUNCTION_ARGS){
	Histogram * htg1 = (Histogram*) PG_GETARG_POINTER(0);
	Histogram * htg2 = (Histogram*) PG_GETARG_POINTER(1);

	CompNumber cn = histogramComp(htg1,htg2);
	CondResult result = false;

	if(cn == SAME) result = true;

	PG_RETURN_BOOL(result);
}

PG_FUNCTION_INFO_V1(htg_lt);

Datum htg_lt(PG_FUNCTION_ARGS){
	Histogram * htg1 = (Histogram*) PG_GETARG_POINTER(0);
	Histogram * htg2 = (Histogram*) PG_GETARG_POINTER(1);

	CompNumber cn = histogramComp(htg1,htg2);
	CondResult result = false;

	if(cn == LESS_THAN) result = true;
	PG_RETURN_BOOL(result);
}

PG_FUNCTION_INFO_V1(htg_lte);

Datum htg_lte(PG_FUNCTION_ARGS){
	Histogram * htg1 = (Histogram*) PG_GETARG_POINTER(0);
	Histogram * htg2 = (Histogram*) PG_GETARG_POINTER(1);

	CompNumber cn = histogramComp(htg1,htg2);
	CondResult result = false;

	if(cn == LESS_THAN || cn == SAME) result = true;
	PG_RETURN_BOOL(result);
}

PG_FUNCTION_INFO_V1(htg_gt);

Datum htg_gt(PG_FUNCTION_ARGS){
	Histogram * htg1 = (Histogram*) PG_GETARG_POINTER(0);
	Histogram * htg2 = (Histogram*) PG_GETARG_POINTER(1);

	CompNumber cn = histogramComp(htg1,htg2);
	CondResult result = false;

	if(cn == GREATER_THAN) result = true;
	PG_RETURN_BOOL(result);
}

PG_FUNCTION_INFO_V1(htg_gte);

Datum htg_gte(PG_FUNCTION_ARGS){
	Histogram * htg1 = (Histogram*) PG_GETARG_POINTER(0);
	Histogram * htg2 = (Histogram*) PG_GETARG_POINTER(1);

	CompNumber cn = histogramComp(htg1,htg2);
	CondResult result = false;

	if(cn == GREATER_THAN || cn == SAME) result = true;
	PG_RETURN_BOOL(result);
}


PG_FUNCTION_INFO_V1(htg_distance);

Datum htg_distance(PG_FUNCTION_ARGS){
	Histogram * htg1 = (Histogram*) PG_GETARG_POINTER(0);
	Histogram * htg2 = (Histogram*) PG_GETARG_POINTER(1);

	Dis result = histogramDistance(htg1,htg2);

	PG_RETURN_FLOAT8(result);
}

