#ifndef HISTOGRAM_H
#define HISTOGRAM_H

#include <stdio.h> 
#include <string.h>
#include <stdlib.h>
#include "postgres.h"
#include "fmgr.h"
#include "math.h"
#include "../Utils/definition.h"
#include <utils/array.h>




typedef struct Histogram{
	VALH htg[HTG_SIZE];
	double radius;

} Histogram;

void generateHistogram(Path path){
	Path com1 = (Path) palloc(strlen(PREFIX) + strlen(SCRIPT) + EXTRA_SPACE);
	com1 = strcpy(com1,PREFIX);
	com1 = strcat(com1,SCRIPT);

	Command comp = (Command) palloc(strlen(com1) + strlen(path) + EXTRA_SPACE);
	comp = strcpy(comp,com1);
	comp = strcat(comp,path);

	//int e = system("/home/xnpio/Documentos/Xnpio/EDA-UNSA/EDA-UNSA/Postgres/hystogramTest/HTG  /home/xnpio/Documentos/Xnpio/EDA-UNSA/EDA-UNSA/Postgres/hystogramTest/00372608.jpeg ");
	SystemError e = system(comp);
	if(e == SYSTEM_ERROR){
		ereport(ERROR,
				(errcode(ERRCODE_INVALID_TEXT_REPRESENTATION),
				 errmsg(ERROR_MSG1)));	
	}

}

Dis histogramDistance(Histogram * htg1, Histogram * htg2){
	Dis result = 0;
	for(Iterator i = 0; i < HTG_SIZE; i++){
		result += pow(htg2->htg[i] - htg1->htg[i], SQUARE);
	}
	return sqrt(result);
}

CompNumber histogramComp(Histogram * htg1, Histogram * htg2){
	Counter less = 0;
	Counter greater = 0;
	Counter same = 0;
	for(Iterator i = 0; i < HTG_SIZE; i++){
		if(htg1->htg[i] == htg2->htg[i]) same++;
		else if(htg1->htg[i] < htg2->htg[i]) less++;
		else greater++;
	}
	if(same == HTG_SIZE) return SAME;
	else if(less >= greater) return LESS_THAN;
	else return GREATER_THAN;
}



#endif