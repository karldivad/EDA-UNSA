#ifndef DEFINITION_H
#define DEFINITION_H

#define BUFFER_SIZE 30
#define INITIAL_RADIUS 0
#define HTG_SIZE 256
#define SAME 0
#define LESS_THAN 1
#define GREATER_THAN 2
#define EXTRA_SPACE 1
#define SYSTEM_ERROR -1
#define SQUARE 2
#define DISTANCE_STRATEGY 15
#define PENALTY_LIMIT 2400000; //2349296 -> 1024 * 768 * 3

typedef bool CondResult;
typedef float Dis;
typedef int VALH;
typedef int SystemError;
typedef int Iterator;
typedef int Counter;
typedef int CompNumber;
typedef char BufferLine;
typedef char * Path;
typedef char * Message;
typedef char * Command;
typedef const Path ConstantPath;
typedef const Message ConstantMessage;
typedef const Command ConstantCommand;


ConstantPath TYPE = ".htg";
ConstantPath PREFIX = "/home/xnpio/Documentos/Xnpio/EDA-UNSA/EDA-UNSA/Postgres/hystogramTest/"; 
ConstantPath SCRIPT = "HTG ";
ConstantCommand REMOVE_COMMAND = "rm ";
ConstantMessage ERROR_MSG2 = "invalid input syntax for histogram: \"%s\" The file doesn't exist";
ConstantMessage ERROR_MSG3 = "The histogram wasn't eraser";
ConstantMessage HTG_MSG_OUT = "First and Last : (%d,%d) - R: %lf";
ConstantMessage ERROR_MSG1 = "The histogram doesn't generate";

#endif