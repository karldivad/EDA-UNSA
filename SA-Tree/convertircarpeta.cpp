#include <iostream>
#include "CImg.h"
#include <fstream>

using namespace std;
using namespace cimg_library;

DIR *dir;
struct dirent *ent;

int main ()
{
  CImg<float>* image;
  CImgDisplay main_disp;
  int n=0;
  if ((dir = opendir ("./")) != NULL) {
  /* print all the files and directories within directory */
    while ((ent = readdir (dir)) != NULL) {
      if(ent->d_name[0]!='.'){
        string united = ent->d_name;
        united +=".htg";
        n++;
        //printf ("%s\n", ent->d_name);
        image = new CImg<float>(ent->d_name);
        ofstream datos(united);
        CImg<float> histogram = image->histogram(256);
        for(int i=0; i<256; i++) datos<<(int)histogram(i,0,0,0)<<'\n';
        datos.close();
        //cout<<(int)histogram(255,0,0,0)<<endl;
        //cout<<(int)histogram(255,0,0,1)<<endl;
        //cout<<(int)histogram(255,0,0,2)<<endl;
        //cout<<(int)histogram(255,0,0,3)<<endl;
        //image->display_graph(0,1);
        if(n%1000==0) cout<<n<<endl;
      }
    }
    closedir (dir);
  }else {
  /* could not open directory */
    perror ("");
    return EXIT_FAILURE;
  }
  //CImg<unsigned char> image;
  //for(
  return 0;
}
