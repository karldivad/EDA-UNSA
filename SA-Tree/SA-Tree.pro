#-------------------------------------------------
#
# Project created by QtCreator 2017-01-09T22:57:24
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SA-Tree
TEMPLATE = app

LIBS += -lX11

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h \
    SATree.h \
    CImg.h

FORMS    += mainwindow.ui

DISTFILES += \
    imagens.dat
