#include <iostream>
#include "CImg.h"
#include <fstream>
//#include <string>

using namespace std;
using namespace cimg_library;

int main (int argc, char* argv[])
{
  if (argc != 2) return 1;

  string united = argv[1];
  //size_t pos = united.find_last_of("/\\");
  //if(pos!=-1) united = united.substr(pos+1);
  united +=".htg";
  
  CImg<float> image(argv[1]);
  CImgDisplay main_disp;

  ofstream datos(united);

  int dimentions = 256;
  CImg<float> histogram = image.histogram(dimentions);
  int sum = 0;
  for(int i=0; i<dimentions; i++) { sum+=(int)histogram(i,0,0,0); datos<<(int)histogram(i,0,0,0)<<'\n';}
  cout<<sum<<endl;
  datos.close();

  //iniTesting
  /*
  cout<<(int)histogram(255,0,0,0)<<endl;
  cout<<(int)histogram(255,0,0,1)<<endl;
  cout<<(int)histogram(255,0,0,2)<<endl;
  cout<<(int)histogram(255,0,0,3)<<endl;
  image.display_graph(0,3);
  */
  //endTesting

  return 0;
}
