#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "SATree.h"
#include <fstream>
#include <string>
#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include "CImg.h"
#include <QPixmap>
#include <QString>

using namespace std;
using namespace cimg_library;

Dis dd(tuple<vector<hisval>,string> a, tuple<vector<hisval>,string> b){
    Dis sum = 0;
    for(int i = 1; i < get<0>(a).size()-1; i++) sum += pow(get<0>(b)[i] - get<0>(a)[i],2);
    return sqrt(sum);
}

MainWindow::MainWindow(QWidget *parent):
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //ifstream images("/home/kd/SA-Tree/imagens.dat");
    ifstream images("/home/xnpio/Documentos/imagens.dat");

    ifstream entering;
    string line;
    string lines;
    //qDebug()<<"lalal";
    for(int i=0;getline(images,line);i++)
    {
      imagenes.push_back(make_tuple(vector<hisval>(),line));
      //entering.open(("/home/kd/CImgDescom/Imagenes/"+line+".htg").c_str());
      entering.open(("/home/xnpio/Documentos/Imagenes/"+line+".htg").c_str());

      for(int j = i;getline(entering,lines);)
      {
        get<0>(imagenes[j]).push_back(stoi(lines));
      }
      entering.close();
    }
    images.close();
    //qDebug()<<"lalala";
    //vector<vector<int>>  points = { {0,0},{1,1},{-2,1},{0,-1},{-3,2},{-4,2},{1,-2},{4,4} );
    SATree<tuple<vector<hisval>,string>>::FunDis distance = dd;
    static SATree<tuple<vector<hisval>,string>> * ST = new SATree<tuple<vector<hisval>,string>>(dd, imagenes);
    //vector<int> query;
    //query.resize(256);
    //SATree<vector<int>>::SetOfKPoints res2 = ST->NNSearch(query,10);
    //ST->printSetOfKPoints(res2);
    //ST->print("muaja");
    //cout<<"lalala"<<endl;
    SATREE = (void*)ST;

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QString filename=QFileDialog::getOpenFileName(
                this,
                tr("Seleccionar Imagen"),
                "/home/xnpio/",
                "Picture Files (*.jpg *.jpeg *.png)"
                );
    QPixmap image(filename);
    ui->label->setPixmap(image);
    search.clear();
    CImg<float> pixel(filename.toStdString().c_str());
    //pixel.resize(240,288);
    pixel = pixel.histogram(256);
    //int sum = 0;
    //for(int i=0;i<256;i++) {sum+=(int)pixel(i,0,0,0);}
    //sum=sum/207360;

    for(int i=0;i<256;i++) {search.push_back((hisval)pixel(i,0,0,0));}
}

void MainWindow::on_pushButton_2_clicked()
{
    SATree<tuple<vector<hisval>,string>> * ST = static_cast<SATree<tuple<vector<hisval>,string>>*>(SATREE);
    SATree<tuple<vector<hisval>,string>>::SetOfKPoints res2 = ST->NNSearch(make_tuple(search,"test"),6);
    //string loc="/home/kd/CImgDescom/Imagenes/";
    string loc="/home/xnpio/Documentos/Imagenes/";
    QPixmap image2(QString::fromStdString(loc+get<1>(res2[0])));
    QPixmap image3(QString::fromStdString(loc+get<1>(res2[1])));
    QPixmap image4(QString::fromStdString(loc+get<1>(res2[2])));
    QPixmap image5(QString::fromStdString(loc+get<1>(res2[3])));
    QPixmap image6(QString::fromStdString(loc+get<1>(res2[4])));
    QPixmap image7(QString::fromStdString(loc+get<1>(res2[5])));

    ui->label_2->setPixmap(image2);
    ui->label_3->setPixmap(image3);
    ui->label_4->setPixmap(image4);
    ui->label_5->setPixmap(image5);
    ui->label_6->setPixmap(image6);
    ui->label_7->setPixmap(image7);
}
