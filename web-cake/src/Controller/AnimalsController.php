<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Animals Controller
 *
 * @property \App\Model\Table\AnimalsTable $Animals
 */
class AnimalsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $animals = $this->paginate($this->Animals);

        $this->set(compact('animals'));
        $this->set('_serialize', ['animals']);
    }

    /**
     * View method
     *
     * @param string|null $id Animal id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $animal = $this->Animals->get($id, [
            'contain' => []
        ]);

        $this->set('animal', $animal);
        $this->set('_serialize', ['animal']);
    }

    /**
     * Search method
     *
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function search()
    {
        $unsa = $this->Animals->newEntity();
        $data = [];
        $tmp = '';
        if($this->request->is('post')){
            $path = '/home/julius/';
            $path .= $this->request->data['image']['name'];
            
            /*select path, (htg <-> '/home/xnpio/Documentos/Imagenes2/00372608.jpeg'::histogram) as dist from unsa order by dist asc limit 10;*/
            
            $this->uploadFile($this->request->data['image']['tmp_name'], $this->request->data['image']['name']);

            $data = $this->Animals->find('all')
                ->select(['path' => 'path', 'dist' => '(htg <-> \'' . $path . '\'::histogram)'])
                ->order(['dist' => 'ASC'])
                ->limit(6);

            $data = $this->downloadFile($data->toArray());
            $tmp = 'files/'.$this->request->data['image']['name'];
            move_uploaded_file($this->request->data['image']['tmp_name'], WWW_ROOT . $tmp);
            $tmp = '/' . $tmp;

        }
        $this->set(compact('unsa', 'data', 'tmp'));
        $this->set('_serialize', ['unsa']);
        //$this->Unsa->find()
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $animal = $this->Animals->newEntity();
        if ($this->request->is('post')) {
            $animal = $this->Animals->patchEntity($animal, $this->request->data);
            if ($this->Animals->save($animal)) {
                $this->Flash->success(__('The animal has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The animal could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('animal'));
        $this->set('_serialize', ['animal']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Animal id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $animal = $this->Animals->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $animal = $this->Animals->patchEntity($animal, $this->request->data);
            if ($this->Animals->save($animal)) {
                $this->Flash->success(__('The animal has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The animal could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('animal'));
        $this->set('_serialize', ['animal']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Animal id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $animal = $this->Animals->get($id);
        if ($this->Animals->delete($animal)) {
            $this->Flash->success(__('The animal has been deleted.'));
        } else {
            $this->Flash->error(__('The animal could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function uploadFile($archivo_local, $archivo_remoto){
        //Sube archivo de la maquina Cliente al Servidor (Comando PUT)
        $id_ftp = $this->ConnectFTP(); //Obtiene un manejador y se conecta al Servidor FTP
        ftp_put($id_ftp, $archivo_remoto, $archivo_local, FTP_BINARY);
        //Sube un archivo al Servidor FTP en modo Binario
        ftp_quit($id_ftp); //Cierra la conexion FTP
    }

    public function downloadFile($files_array){

        $id_ftp = $this->ConnectFTP(); //Obtiene un manejador y se conecta al 
        $data = [];
        foreach ($files_array as $key => $value) {
            $local_file = explode('/', $value['path'], 6)[5];
            $d = []; 
            ftp_get($id_ftp, WWW_ROOT.'files/'.$local_file, $value['path'], FTP_BINARY);
            $d['path'] = '/files/'.$local_file;
            $d['dist'] = $value['dist'];
            array_push($data, $d);
        }
        ftp_quit($id_ftp); //Cierra la conexion FTP   
        return $data;
    }

    public function ConnectFTP(){
    //Permite conectarse al Servidor FTP
        $id_ftp = ftp_connect('192.168.43.173', 21); //Obtiene un manejador del Servidor FTP
        ftp_login($id_ftp, 'julius', 'root'); //Se loguea al Servidor FTP
        ftp_pasv($id_ftp, true); //Establece el modo de conexión
        return $id_ftp; //Devuelve el manejador a la función
    }
}
