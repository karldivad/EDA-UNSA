<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Flags Controller
 *
 * @property \App\Model\Table\FlagsTable $Flags
 */
class FlagsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $flags = $this->paginate($this->Flags);

        $this->set(compact('flags'));
        $this->set('_serialize', ['flags']);
    }

    /**
     * View method
     *
     * @param string|null $id Flag id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $flag = $this->Flags->get($id, [
            'contain' => []
        ]);

        $this->set('flag', $flag);
        $this->set('_serialize', ['flag']);
    }

    /**
     * Search method
     *
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function search()
    {
        $unsa = $this->Flags->newEntity();
        $data = [];
        $tmp = '';
        if($this->request->is('post')){
            $path = '/home/julius/';
            $path .= $this->request->data['image']['name'];
            
            /*select path, (htg <-> '/home/xnpio/Documentos/Imagenes2/00372608.jpeg'::histogram) as dist from unsa order by dist asc limit 10;*/
            
            $this->uploadFile($this->request->data['image']['tmp_name'], $this->request->data['image']['name']);

            $data = $this->Flags->find('all')
                ->select(['path' => 'path', 'dist' => '(htg <-> \'' . $path . '\'::histogram)'])
                ->order(['dist' => 'ASC'])
                ->limit(6);

            $data = $this->downloadFile($data->toArray());
            $tmp = 'files/'.$this->request->data['image']['name'];
            move_uploaded_file($this->request->data['image']['tmp_name'], WWW_ROOT . $tmp);
            $tmp = '/' . $tmp;

        }
        $this->set(compact('unsa', 'data', 'tmp'));
        $this->set('_serialize', ['unsa']);
        //$this->Unsa->find()
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $flag = $this->Flags->newEntity();
        if ($this->request->is('post')) {
            $flag = $this->Flags->patchEntity($flag, $this->request->data);
            if ($this->Flags->save($flag)) {
                $this->Flash->success(__('The flag has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The flag could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('flag'));
        $this->set('_serialize', ['flag']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Flag id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $flag = $this->Flags->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $flag = $this->Flags->patchEntity($flag, $this->request->data);
            if ($this->Flags->save($flag)) {
                $this->Flash->success(__('The flag has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The flag could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('flag'));
        $this->set('_serialize', ['flag']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Flag id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $flag = $this->Flags->get($id);
        if ($this->Flags->delete($flag)) {
            $this->Flash->success(__('The flag has been deleted.'));
        } else {
            $this->Flash->error(__('The flag could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function uploadFile($archivo_local, $archivo_remoto){
        //Sube archivo de la maquina Cliente al Servidor (Comando PUT)
        $id_ftp = $this->ConnectFTP(); //Obtiene un manejador y se conecta al Servidor FTP
        ftp_put($id_ftp, $archivo_remoto, $archivo_local, FTP_BINARY);
        //Sube un archivo al Servidor FTP en modo Binario
        ftp_quit($id_ftp); //Cierra la conexion FTP
    }

    public function downloadFile($files_array){

        $id_ftp = $this->ConnectFTP(); //Obtiene un manejador y se conecta al 
        $data = [];
        foreach ($files_array as $key => $value) {
            $local_file = explode('/', $value['path'], 6)[5];
            $d = []; 
            ftp_get($id_ftp, WWW_ROOT.'files/'.$local_file, $value['path'], FTP_BINARY);
            $d['path'] = '/files/'.$local_file;
            $d['dist'] = $value['dist'];
            array_push($data, $d);
        }
        ftp_quit($id_ftp); //Cierra la conexion FTP   
        return $data;
    }

    public function ConnectFTP(){
    //Permite conectarse al Servidor FTP
        $id_ftp = ftp_connect('192.168.43.173', 21); //Obtiene un manejador del Servidor FTP
        ftp_login($id_ftp, 'julius', 'root'); //Se loguea al Servidor FTP
        ftp_pasv($id_ftp, true); //Establece el modo de conexión
        return $id_ftp; //Devuelve el manejador a la función
    }
}
