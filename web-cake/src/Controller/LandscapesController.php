<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Landscapes Controller
 *
 * @property \App\Model\Table\LandscapesTable $Landscapes
 */
class LandscapesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $landscapes = $this->paginate($this->Landscapes);

        $this->set(compact('landscapes'));
        $this->set('_serialize', ['landscapes']);
    }

    /**
     * View method
     *
     * @param string|null $id Landscape id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $landscape = $this->Landscapes->get($id, [
            'contain' => []
        ]);

        $this->set('landscape', $landscape);
        $this->set('_serialize', ['landscape']);
    }

    /**
     * Search method
     *
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function search()
    {
        $unsa = $this->Landscapes->newEntity();
        $data = [];
        $tmp = '';
        if($this->request->is('post')){
            $path = '/home/julius/';
            $path .= $this->request->data['image']['name'];
            
            /*select path, (htg <-> '/home/xnpio/Documentos/Imagenes2/00372608.jpeg'::histogram) as dist from unsa order by dist asc limit 10;*/
            
            $this->uploadFile($this->request->data['image']['tmp_name'], $this->request->data['image']['name']);

            $data = $this->Landscapes->find('all')
                ->select(['path' => 'path', 'dist' => '(htg <-> \'' . $path . '\'::histogram)'])
                ->order(['dist' => 'ASC'])
                ->limit(6);

            $data = $this->downloadFile($data->toArray());
            $tmp = 'files/'.$this->request->data['image']['name'];
            move_uploaded_file($this->request->data['image']['tmp_name'], WWW_ROOT . $tmp);
            $tmp = '/' . $tmp;

        }
        $this->set(compact('unsa', 'data', 'tmp'));
        $this->set('_serialize', ['unsa']);
        //$this->Unsa->find()
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $landscape = $this->Landscapes->newEntity();
        if ($this->request->is('post')) {
            $landscape = $this->Landscapes->patchEntity($landscape, $this->request->data);
            if ($this->Landscapes->save($landscape)) {
                $this->Flash->success(__('The landscape has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The landscape could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('landscape'));
        $this->set('_serialize', ['landscape']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Landscape id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $landscape = $this->Landscapes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $landscape = $this->Landscapes->patchEntity($landscape, $this->request->data);
            if ($this->Landscapes->save($landscape)) {
                $this->Flash->success(__('The landscape has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The landscape could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('landscape'));
        $this->set('_serialize', ['landscape']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Landscape id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $landscape = $this->Landscapes->get($id);
        if ($this->Landscapes->delete($landscape)) {
            $this->Flash->success(__('The landscape has been deleted.'));
        } else {
            $this->Flash->error(__('The landscape could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function uploadFile($archivo_local, $archivo_remoto){
        //Sube archivo de la maquina Cliente al Servidor (Comando PUT)
        $id_ftp = $this->ConnectFTP(); //Obtiene un manejador y se conecta al Servidor FTP
        ftp_put($id_ftp, $archivo_remoto, $archivo_local, FTP_BINARY);
        //Sube un archivo al Servidor FTP en modo Binario
        ftp_quit($id_ftp); //Cierra la conexion FTP
    }

    public function downloadFile($files_array){

        $id_ftp = $this->ConnectFTP(); //Obtiene un manejador y se conecta al 
        $data = [];
        foreach ($files_array as $key => $value) {
            $local_file = explode('/', $value['path'], 6)[5];
            $d = []; 
            ftp_get($id_ftp, WWW_ROOT.'files/'.$local_file, $value['path'], FTP_BINARY);
            $d['path'] = '/files/'.$local_file;
            $d['dist'] = $value['dist'];
            array_push($data, $d);
        }
        ftp_quit($id_ftp); //Cierra la conexion FTP   
        return $data;
    }

    public function ConnectFTP(){
    //Permite conectarse al Servidor FTP
        $id_ftp = ftp_connect('192.168.43.173', 21); //Obtiene un manejador del Servidor FTP
        ftp_login($id_ftp, 'julius', 'root'); //Se loguea al Servidor FTP
        ftp_pasv($id_ftp, true); //Establece el modo de conexión
        return $id_ftp; //Devuelve el manejador a la función
    }
}
