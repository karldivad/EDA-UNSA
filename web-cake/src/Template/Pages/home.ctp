<!-- Intro Header -->
<header class="intro">
    <div class="intro-body">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h1 class="brand-heading">Evil-Twin</h1>
                    <h2 class="intro-text text-primary" style="text-shadow: 2px 2px white">Busca a tu gemelo malvado
                        <br>Powered by ZealotSoft</h2>

                        <?= $this->Form->create(null, ['url' => ['controller' => 'Unsa', 'action' => 'search'], 'type' => 'file', 'id' => 'form-search'])?>
                            
                            <?= $this->Form->input('image', ['label' => false, 'class'=>'form-control input-file', 'type'=>'file', 'accept'=>'image/*', 'required']) ?>
                            </br>

                            <?php 
                                $options = [$this->Url->build(['controller' => 'Unsa', 'action' => 'search']) => 'Alumnos UNSA', $this->Url->build(['controller' => 'Landscapes', 'action' => 'search']) => 'Paisajes', $this->Url->build(['controller' => 'Animals', 'action' => 'search']) => 'Animales', $this->Url->build(['controller' => 'Flags', 'action' => 'search']) => 'Banderas'];
                            ?>
                            <?= $this->Form->select('table', $options, ['empty' => false, 'class' => 'form-control', 'style' => 'width: 200px', 'id' => 'id-table', 'onchange' => 'changeTable();']) ?>
                            
                            <?= $this->Form->button('Buscar Ahora', ['class' => 'btn btn-success btn-lg btn-border']); ?>
                            </br>
                        <?= $this->Form->end()?>

                    <?php if($tmp != ''): ?>
                        <?= $this->Html->image($tmp, ['class' => 'img-thumbnail btn-border', 'style' => 'width: 110px']); ?>
                    <?php endif; ?>
                    </br>

                    <?php foreach ($data as $key => $value): ?>
                        <?= $this->Html->image($value['path'], ['class' => 'img-thumbnail', 'style' => 'width: 110px']); ?>
                    <?php endforeach; ?>
                    </br>
                    <a href="#about" class="btn btn-circle page-scroll">
                        <i class="fa fa-angle-double-down animated"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>

