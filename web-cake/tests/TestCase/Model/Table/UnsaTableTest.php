<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UnsaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UnsaTable Test Case
 */
class UnsaTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UnsaTable
     */
    public $Unsa;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.unsa'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Unsa') ? [] : ['className' => 'App\Model\Table\UnsaTable'];
        $this->Unsa = TableRegistry::get('Unsa', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Unsa);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
