<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LandscapesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LandscapesTable Test Case
 */
class LandscapesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LandscapesTable
     */
    public $Landscapes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.landscapes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Landscapes') ? [] : ['className' => 'App\Model\Table\LandscapesTable'];
        $this->Landscapes = TableRegistry::get('Landscapes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Landscapes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
